# 单页应用路由

#### 介绍
原生js实现一个单页应用路由

具体说明：https://www.cnblogs.com/clis/p/14672179.html

演示地址：https://lisheng741.gitee.io/singlepagerouter/Index.html


#### 安装教程
Router.js 文件即可

#### 使用说明

1、整个应用生命周期，只创建一次路由器，即 var router = new Router() 时，只 new 一次。（否则会注册多次 hashchange 事件）

2、路由器创建以后，可以使用 router.init() 切换路由表

3、控制 router 跳转路由使用 router.push(route) 方法，输入的 route 可以是路由表的下标，也可以是路由表 id 的字符串，也可以是 route 对象，若没有输入，则默认不会跳转。


#### 提交 bug 和其他需求

issues 告诉我，如果是 bug，我会很乐意解决的~



